# CSV PARSER MICROSERVICE  
This project is part of **[K8S Log Server](https://bitbucket.org/syngress/k8s-log-server/src/master)**    

![JavaLogo](https://syngress.pl/images/kubernetes_log_server/java_logo.png)
![SpringBootLogo](https://syngress.pl/images/kubernetes_log_server/spring_boot_logo.png)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Things you may want to cover for local development:

* Java  
  `14.0.2`

* Spring  
  `2.1.1`

* System dependencies and Services (job queues, cache servers, search engines, etc.)  
  `MongoDB 4.4.2`  
  `RabbitMQ 3.8.9`  
  `Docker 20.10.0`  
  `Maven 3.6.3`

* Configuration  
  `Configure path to your csv log directory, default is /tmp`  
  `Setup RabbitMQ application account`

When docker-compose create containers, we can log in to rabbit service through gui.  
Enter `localhost:15672` and log in as admin with default login: `admin` and password: `admin`  
You should see the following screen:  

![SpringBootLogo](https://syngress.pl/images/kubernetes_log_server/rabbit_1.png)

Now got to `Admin` tab and click `Add a user`  

![SpringBootLogo](https://syngress.pl/images/kubernetes_log_server/rabbit_2.png)

Check `application.yml` configuration file, defines information about the user who will connect to the
rabbitmq service.  
Default username: `csvpuser` and password `springboot`, add a user by filling out the form, and
click `Add user`.  
Remember that the newly created user does not have any permissions ! We need to edit our new user
and add some permissions.  

![SpringBootLogo](https://syngress.pl/images/kubernetes_log_server/rabbit_3.png)

Click on the user name (table column `Name`) you should see the following screen:

![SpringBootLogo](https://syngress.pl/images/kubernetes_log_server/rabbit_4.png)

All you have to do is click the two marked buttons, and you ready to start parser application.

* Application Initialization  
  `mvn clean install`

**[Log Analyzer Microservice](https://bitbucket.org/syngress/k8s-log-analyzer/src/master/)** generate CSV file and send
parse request to this microservice.  
Application reads CSV from a configured location, and based on the data contained in the CSV file, JSON working file is generated.  
If JSON file is generated correctly, each of its fields is saved in the mongo database as a separate document.  
For each parse request, a unique `id` is created and saved with each mongo document as `parser_id` field, this makes it easier to search for information logged for a specific day.  
The data will be processed in the Rabbitmq queue.  

Example response from parse request:  

```json
{
    "_id" : ObjectId("5febcef4ff88c360b5da25d9"),
    "log_file_name" : "2020.10.24.csv",
    "log_lines" : "60",
    "_class" : "parserpackage.model.entity.Parser"
}
```

Sample mongo document:  

```json
{
    "_id" : ObjectId("5ff2463a0a58940e2414573c"),
    "parser_id" : "5febcef4ff88c360b5da25d9",
    "source_ip" : "192.168.2.111",
    "hardware_name" : "hardware_one",
    "destination_ip" : "17.212.108.125",
    "destination_name" : "AS714 APPLE-ENGINEERING",
    "connection_time" : "Oct 24 14:42:44"
}
```

In the next step application verifies the configuration file and checks if the data saved in mongodb must be converted into an xml file.  

# Table of endpoint

CSV PARSER

|Method          |Endpoint                                                                                         |Note                                       |
|----------------|-------------------------------------------------------------------------------------------------|-------------------------------------------|
|POST			 |`/v1/parse_csv`            	                                                                   |Parse CSV file and save data to mongodb    |
|GET             |`/v1/connections`                                                                                |Get all connections collection             |
|GET			 |`/v1/connections?parser_id=5ffb25a8aa55e424a2ab54e2`                                             |Get parsed connections collection  	       |
|GET             |`/v1/connections?parser_id=5ffe194e29f2600afe009910&page=0&size=3&sort=source_ip&direction=DESC` |Get parsed connections collection pageable |

# Sequence Diagram

![LogAnalyzerUML](https://syngress.pl/images/kubernetes_log_server/CsvParserUML.png)  

**Free Software, Hell Yeah!**

**THIS PROJECT IS STILL UNDER CONSTRUCTION**
