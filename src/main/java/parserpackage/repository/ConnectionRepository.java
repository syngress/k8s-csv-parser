package parserpackage.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import parserpackage.model.entity.Connection;

public interface ConnectionRepository extends MongoRepository<Connection, String> {
    Connection findById(ObjectId id);
    Page<Connection> findByParserId(String parser_id, Pageable pageable);
}
