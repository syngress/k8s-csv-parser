package parserpackage.repository;

import parserpackage.model.entity.Parser;
import org.bson.types.ObjectId;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ParserRepository extends MongoRepository<Parser, String> {
    Parser findById(ObjectId id);

}
