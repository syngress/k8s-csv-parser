package parserpackage.model.entity;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.annotation.Id;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "#{@environment.getProperty('mongo.collection.connection')}")
public class Connection {
    @Id
    private ObjectId id;
    @Field("parser_id")
    private String parserId;
    private String source_ip;
    private String hardware_name;
    private String destination_ip;
    private String destination_name;
    private String connection_time;

    public Connection() {}

    public Connection(
            ObjectId id,
            String parser_id,
            String source_ip,
            String hardware_name,
            String destination_ip,
            String destination_name,
            String connection_time
    ) {
        this.id = id;
        this.parserId = parser_id;
        this.source_ip = source_ip;
        this.hardware_name = hardware_name;
        this.destination_ip = destination_ip;
        this.destination_name = destination_name;
        this.connection_time = connection_time;
    }

    public String getId() {
        return id.toHexString();
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getParser_id() {
        return parserId;
    }

    public void setParser_id(String parser_id) {
        this.parserId = parser_id;
    }

    public String getSource_ip() {
        return source_ip;
    }

    public void setSource_ip(String source_ip) {
        this.source_ip = source_ip;
    }

    public String getHardware_name() {
        return hardware_name;
    }

    public void setHardware_name(String hardware_name) {
        this.hardware_name = hardware_name;
    }

    public String getDestination_ip() {
        return destination_ip;
    }

    public void setDestination_ip(String destination_ip) {
        this.destination_ip = destination_ip;
    }

    public String getDestination_name() {
        return destination_name;
    }

    public void setDestination_name(String destination_name) {
        this.destination_name = destination_name;
    }

    public String getConnection_time() {
        return connection_time;
    }

    public void setConnection_time(String connection_time) {
        this.connection_time = connection_time;
    }

}
