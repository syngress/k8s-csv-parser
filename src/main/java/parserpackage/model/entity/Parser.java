package parserpackage.model.entity;

import org.springframework.data.annotation.Id;
import org.bson.types.ObjectId;

public class Parser {
    @Id
    private ObjectId id;
    private String log_file_name;
    private String log_lines;

    public Parser() {}

    public Parser(
            ObjectId id,
            String log_file_name,
            String log_lines
    ) {
        this.id = id;
        this.log_file_name = log_file_name;
        this.log_lines = log_lines;
    }

    public String getId() {
        return id.toHexString();
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getLog_file_name() {
        return log_file_name;
    }

    public void setLog_file_name(String log_file_name) {
        this.log_file_name = log_file_name;
    }

    public String getLog_lines() {
        return log_lines;
    }

    public void setLog_lines(String log_lines) {
        this.log_lines = log_lines;
    }

}
