package parserpackage.model.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.mongodb.core.index.Indexed;

public class ConnectionVO {
    @JsonProperty("id")
    @Indexed(unique = true)
    @ApiModelProperty(required = true, value = "Object ID")
    public String id;

    @JsonProperty("parser_id")
    @ApiModelProperty(required = true, value = "Parser ID")
    public String parser_id;

    @JsonProperty("source_ip")
    @ApiModelProperty(required = true, value = "Source IP")
    public String source_ip;

    @JsonProperty("hardware_name")
    @ApiModelProperty(required = true, value = "Local Hardware Name")
    public String hardware_name;

    @JsonProperty("destination_ip")
    @ApiModelProperty(required = true, value = "Destination IP")
    public String destination_ip;

    @JsonProperty("destination_name")
    @ApiModelProperty(required = true, value = "Destination Name")
    public String destination_name;

    @JsonProperty("connection_time")
    @ApiModelProperty(required = true, value = "Connection Time")
    public String connection_time;

}
