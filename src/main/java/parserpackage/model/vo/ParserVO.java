package parserpackage.model.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.mongodb.core.index.Indexed;

public class ParserVO {
    @JsonProperty("id")
    @Indexed(unique = true)
    @ApiModelProperty(required = true, value = "Object ID")
    public String id;

    @JsonProperty("log_file_name")
    @ApiModelProperty(required = true, value = "Log File Name")
    public String log_file_name;

    @JsonProperty("log_lines")
    @ApiModelProperty(required = true, value = "Log Lines")
    public String log_lines;
}
