package parserpackage.model.converter;

import parserpackage.model.entity.Connection;
import parserpackage.model.vo.ConnectionVO;
import org.bson.types.ObjectId;

public class ConnectionConverter {
    private ConnectionConverter() {}

    public static Connection toConnectionConverter(ConnectionVO vo) {
        Connection connection = new Connection();

        connection.setId(new ObjectId());
        connection.setParser_id(vo.parser_id);
        connection.setSource_ip(vo.source_ip);
        connection.setHardware_name(vo.hardware_name);
        connection.setDestination_ip(vo.destination_ip);
        connection.setDestination_name(vo.destination_name);
        connection.setConnection_time(vo.connection_time);

        return connection;
    }

    public static ConnectionVO toValueObject(Connection connection) {
        ConnectionVO vo = new ConnectionVO();

        vo.id = connection.getId();
        vo.parser_id = connection.getParser_id();
        vo.source_ip = connection.getSource_ip();
        vo.hardware_name = connection.getHardware_name();
        vo.destination_ip = connection.getDestination_ip();
        vo.destination_name = connection.getDestination_name();
        vo.connection_time = connection.getConnection_time();

        return vo;
    }

}
