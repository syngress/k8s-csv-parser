package parserpackage.model.converter;

import parserpackage.model.entity.Parser;
import parserpackage.model.vo.ParserVO;
import org.bson.types.ObjectId;

public class ParserConverter {

    private ParserConverter() {}

    public static Parser toParserConverter(ParserVO vo) {
        Parser parser = new Parser();

        parser.setId(new ObjectId());
        parser.setLog_file_name(vo.log_file_name);
        parser.setLog_lines(vo.log_lines);

        return parser;
    }

    public static ParserVO toValueObject(Parser parser) {
        ParserVO vo = new ParserVO();

        vo.id = parser.getId();
        vo.log_file_name = parser.getLog_file_name();
        vo.log_lines = parser.getLog_lines();

        return vo;
    }

}
