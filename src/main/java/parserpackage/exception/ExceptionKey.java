package parserpackage.exception;

import org.springframework.http.HttpStatus;

public enum ExceptionKey {
    created(HttpStatus.CREATED),                            //201
    noContent(HttpStatus.NO_CONTENT),                       //204
    badRequest(HttpStatus.BAD_REQUEST),                     //400
    unauthorized(HttpStatus.UNAUTHORIZED),                  //401
    forbidden(HttpStatus.FORBIDDEN),                        //403
    notFound(HttpStatus.NOT_FOUND),                         //404
    unprocessableEntity(HttpStatus.UNPROCESSABLE_ENTITY),   //422
    tooManyRequests(HttpStatus.TOO_MANY_REQUESTS),          //429
    serverError(HttpStatus.INTERNAL_SERVER_ERROR),          //500
    serviceUnavailable(HttpStatus.SERVICE_UNAVAILABLE);     //503

    private HttpStatus httpStatus;

    ExceptionKey(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus() {
        return this.httpStatus;
    }

}
