package parserpackage.exception;

public class CommonExceptionResponse {
    private String error;
    private String exception;

    public CommonExceptionResponse(String error, String exception) {
        this.error = error;
        this.exception = exception;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

}
