package parserpackage.exception;

import org.springframework.http.HttpStatus;

public class CommonRestException extends RuntimeException {
    private HttpStatus httpStatus;
    private String message;
    private String description;

    public CommonRestException(ExceptionKey exceptionKey, String description) {
        this(exceptionKey.getHttpStatus(), exceptionKey.name(), description);
    }

    public CommonRestException(HttpStatus status, String message, String description) {
        this.httpStatus = status;
        this.message = message;
        this.description = description;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
