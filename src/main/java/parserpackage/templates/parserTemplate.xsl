<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
    </xsl:template>
    <xsl:template match="connection">
        <networkConnection>
            <id>
                <xsl:value-of select="id" />
            </id>
            <parserId>
                <xsl:value-of select="parser_id" />
            </parserId>
            <sourceIp>
                <xsl:value-of select="source_ip" />
            </sourceIp>
            <hardwareName>
                <xsl:value-of select="hardware_name" />
            </hardwareName>
            <destinationIp>
                <xsl:value-of select="destination_ip" />
            </destinationIp>
            <destinationName>
                <xsl:value-of select="destination_name" />
            </destinationName>
            <timestamp>
                <xsl:value-of select="connection_time" />
            </timestamp>
        </networkConnection>
    </xsl:template>
</xsl:stylesheet>