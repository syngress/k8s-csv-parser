package parserpackage.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import static org.apache.commons.lang3.StringUtils.EMPTY;

import parserpackage.exception.CommonRestException;
import parserpackage.exception.CommonExceptionResponse;

@ControllerAdvice
public class ExceptionControllerAdvice extends ResponseEntityExceptionHandler {
    @ExceptionHandler(value = CommonRestException.class)
    public ResponseEntity<CommonExceptionResponse> handleRestException(final CommonRestException cre) {
        CommonExceptionResponse commonExceptionResponse = new CommonExceptionResponse(cre.getMessage(), cre.getDescription());
        return new ResponseEntity<>(commonExceptionResponse, cre.getHttpStatus());
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<CommonExceptionResponse> handleOtherException(final Exception e) {
        String message = e.getMessage() == null ? EMPTY : e.getMessage();
        CommonExceptionResponse commonExceptionResponse = new CommonExceptionResponse("Other exception", message);
        return new ResponseEntity<>(commonExceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
