package parserpackage.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import org.springframework.data.domain.Page;

import parserpackage.api.ConnectionControllerApi;
import parserpackage.model.converter.ConnectionConverter;
import parserpackage.model.entity.Connection;
import parserpackage.model.vo.ConnectionVO;
import parserpackage.service.connection.ConnectionService;

@RestController
public class ConnectionController implements ConnectionControllerApi {
    @Autowired
    private ConnectionService connectionService;

    public Page<ConnectionVO> findByParserId(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "30") int size,
            @RequestParam(value = "sort", defaultValue = "timestamp", required = false) String sort,
            @RequestParam(value = "direction", defaultValue = "DESC", required = false) String direction,
            @RequestParam(value = "parser_id", required = false) String parser_id) {
        final Page<Connection> connection = connectionService.findByParserId(page, size, sort, direction, parser_id);

        return connection.map(ConnectionConverter::toValueObject);
    }

    public ConnectionVO createConnection(@RequestBody(required = true) @Valid ConnectionVO connectionVO) {
        Connection connection = connectionService.createConnection(ConnectionConverter.toConnectionConverter(connectionVO));

        return ConnectionConverter.toValueObject(connection);
    }

}
