package parserpackage.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.bson.types.ObjectId;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import javax.validation.Valid;

import parserpackage.api.ParserControllerApi;
import parserpackage.model.converter.ParserConverter;
import parserpackage.model.entity.Parser;
import parserpackage.model.vo.ParserVO;
import parserpackage.service.parser.ParserService;

@RestController
public class ParserController implements ParserControllerApi {
    @Autowired
    private ParserService service;

    public ParserVO parseCsv(@RequestBody(required = true) @Valid ParserVO parserVO) {
        Parser parser = service.parseCsv(ParserConverter.toParserConverter(parserVO));
        return ParserConverter.toValueObject(parser);
    }

    public ParserVO findById(@PathVariable("id") ObjectId id) {
        Parser parser = service.findById(id);

        return ParserConverter.toValueObject(parser);
    }

    public Page<ParserVO> getParsedCsvCollection(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "30") int size,
            @RequestParam(value = "sort", defaultValue = "timestamp", required = false) String sort,
            @RequestParam(value = "direction", defaultValue = "DESC", required = false) String direction) {
        final Page<Parser> connection = service.getParsedCsvCollection(page, size, sort, direction);

        return connection.map(ParserConverter::toValueObject);
    }

}
