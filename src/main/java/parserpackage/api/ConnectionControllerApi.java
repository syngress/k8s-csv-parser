package parserpackage.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import javax.validation.Valid;

import parserpackage.model.vo.ConnectionVO;
import parserpackage.model.ErrorResponse;

@Api(value = "v1")
public interface ConnectionControllerApi {
    // Get Connection Pageable
    @ApiOperation(value = "", nickname = "findByParserId", notes = "GetConnectionCollection",
            response = ConnectionVO.class, tags = {"get"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = ConnectionVO.class),
            @ApiResponse(code = 400, message = "BadRequest", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "NotFound", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "ServerError", response = ErrorResponse.class)
    })
    @GetMapping("/v1/connections")
    Page<ConnectionVO> findByParserId(
            int page, int size, String sort, String direction, @RequestParam String parser_id
    );

    // Create Connection Object
    @ApiOperation(value = "", nickname = "createConnection", notes = "Create Connection Object",
            response = ConnectionVO.class, tags = {"create"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = ConnectionVO.class),
            @ApiResponse(code = 400, message = "BadRequest", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "NotFound", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "ServerError", response = ErrorResponse.class)})
    @PostMapping("/v1/create_connection")
    ConnectionVO createConnection(@RequestBody(required = true) @Valid ConnectionVO connectionVO);

}
