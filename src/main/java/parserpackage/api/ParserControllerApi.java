package parserpackage.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.bson.types.ObjectId;
import javax.validation.Valid;

import parserpackage.model.vo.ParserVO;
import parserpackage.model.ErrorResponse;

@Api(value = "v1")
public interface ParserControllerApi {
    // Parse CSV File
    @ApiOperation(value = "", nickname = "parseCsvFile", notes = "Parse CSV File",
            response = ParserVO.class, tags = {"create"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = ParserVO.class),
            @ApiResponse(code = 400, message = "BadRequest", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "NotFound", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "ServerError", response = ErrorResponse.class)})
    @PostMapping("/v1/parse_csv")
    ParserVO parseCsv(@RequestBody(required = true) @Valid ParserVO parserVO);

    // Get Parsed CSV Object by ID
    @ApiOperation(value = "", nickname = "getParsedCsv", notes = "Get Parsed Csv",
            response = ParserVO.class, tags = {"get"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = ParserVO.class),
            @ApiResponse(code = 400, message = "BadRequest", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "NotFound", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "ServerError", response = ErrorResponse.class)
    })
    @GetMapping("/v1/parsed_csv/{id}")
    ParserVO findById(ObjectId id);

    // Get Parsed CSV Collection Pageable
    @ApiOperation(value = "", nickname = "getParsedCsvCollection", notes = "Get Parsed CSV Collection",
            response = ParserVO.class, tags = {"get"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = ParserVO.class),
            @ApiResponse(code = 400, message = "BadRequest", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "NotFound", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "ServerError", response = ErrorResponse.class)
    })
    @GetMapping("/v1/parsed_csv_collection")
    Page<ParserVO> getParsedCsvCollection(
            int page, int size, String sort, String direction
    );

}
