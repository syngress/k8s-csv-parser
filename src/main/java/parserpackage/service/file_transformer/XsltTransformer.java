package parserpackage.service.file_transformer;

import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import java.io.*;

public class XsltTransformer {

    public static void main(String[] args) {
        String XSLFILE = "src/main/java/parserpackage/templates/parserTemplate.xsl";
        String INPUTFILE = args[0];
        String OUTPUTFILE = "target/network_connections.xml";

        StreamSource xslcode =
                new StreamSource(new File(XSLFILE));
        StreamSource input =
                new StreamSource(new File(INPUTFILE));
        StreamResult output =
                new StreamResult(new File(OUTPUTFILE));

        TransformerFactory tf = TransformerFactory.newInstance();

        Transformer trans;
        try {
            trans = tf.newTransformer(xslcode);
            trans.transform(input, output);
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

}
