package parserpackage.service.file_transformer;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Map;

public class CsvToJson {
    public void convert(String fileName, String parserWorkDirectory, String outputJsonFile) {
        createJson(fileName, parserWorkDirectory, outputJsonFile);
    }

    private void createJson(String filename, String parserWorkDirectory, String outputJsonFile) {
        String jsonFilePath = parserWorkDirectory + "/" + outputJsonFile;
        Path csvWorkDirectory = FileSystems.getDefault().getPath(parserWorkDirectory);
        String csvFileName = csvWorkDirectory + "/" + filename;
        File inputFile = new File(csvFileName);
        File outputFile = new File(jsonFilePath);
        CsvSchema csvSchema = CsvSchema.builder().setUseHeader(true).build();
        CsvMapper csvMapper = new CsvMapper();
        ObjectMapper mapper = new ObjectMapper();

        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

        try {
            mapper.writerWithDefaultPrettyPrinter()
                    .writeValue(outputFile,csvMapper.readerFor(Map.class)
                            .with(csvSchema).readValues(inputFile)
                            .readAll()
                    );
            clearCsvFile(inputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void clearCsvFile(File inputFile) {
        inputFile.delete();
    }

}
