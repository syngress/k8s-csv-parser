package parserpackage.service.file_transformer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class RemoveEmptyRows {
    public static void main(String fileName, String csvPath, String parserWorkDirectory) {
        Scanner file;
        PrintWriter writer;

        try {
            file = new Scanner(new File(csvPath + "/" + fileName));
            writer = new PrintWriter(parserWorkDirectory + "/" + fileName);

            while (file.hasNext()) {
                String line = file.nextLine();
                if (!line.isEmpty()) {
                    writer.write(line);
                    writer.write("\n");
                }
            }

            file.close();
            writer.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
