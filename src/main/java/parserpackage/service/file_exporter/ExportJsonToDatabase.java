package parserpackage.service.file_exporter;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.bson.types.ObjectId;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import org.bson.Document;

public class ExportJsonToDatabase {
    @SuppressWarnings("unchecked")
    public void prepare(String databaseConfiguration, String parser_id, String parserWorkDirectory, String outputJsonFile)
    {
        String jsonOutputPath = parserWorkDirectory + "/" + outputJsonFile;
        var mongoClient = MongoClients.create();
        var database = mongoClient.getDatabase(databaseConfiguration);
        MongoCollection<Document> collection = database.getCollection("connectionData");

        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(jsonOutputPath))
        {
            Object obj = jsonParser.parse(reader);
            JSONArray connectionList = (JSONArray) obj;
            connectionList.forEach( con -> storeConnectionObject( (JSONObject) con, collection, parser_id));
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
    }

    private static void storeConnectionObject(JSONObject jsonObject, MongoCollection<Document> collection, String parser_id)
    {
        ObjectId objectId = new ObjectId();
        Document message = new Document()
                .append("_id", objectId)
                .append("parser_id", parser_id)
                .append("source_ip", jsonObject.get("SourceIP"))
                .append("hardware_name", jsonObject.get("HardwareName"))
                .append("destination_ip", jsonObject.get("DestinationIP"))
                .append("destination_name", jsonObject.get("DestinationName"))
                .append("connection_time", jsonObject.get("ConnectionTime"));

        collection.insertOne(message);
    }

}
