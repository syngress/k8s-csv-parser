package parserpackage.service.parser;

import org.bson.types.ObjectId;
import parserpackage.model.entity.Parser;
import org.springframework.data.domain.Page;

public interface ParserService {
    Parser parseCsv(Parser parser);
    Parser findById(ObjectId id);
    Page<Parser> getParsedCsvCollection(int page, int size, String sort, String direction);

}
