package parserpackage.service.parser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.bson.types.ObjectId;

import parserpackage.model.entity.Parser;
import parserpackage.repository.ParserRepository;
import parserpackage.service.rabbitmq.ParserMessageSender;
import parserpackage.validator.Validate;

@Service
public class ParserServiceImpl implements ParserService {
    @Autowired
    private ParserRepository parserRepository;

    @Autowired
    private ParserMessageSender parserMessageSender;

    @Value("${spring.data.mongodb.database}")
    private String databaseConfiguration;

    @Value("${csv.path}")
    private String csvPath;

    @Override
    @Transactional
    public Parser parseCsv(Parser parser) {
        parserMessageSender.sendMessageData(parser, csvPath, databaseConfiguration);
        parserRepository.save(parser);

        return parser;
    }

    @Override
    @Transactional
    public Parser findById(ObjectId id) {
        Parser parserObject = parserRepository.findById(id);
        validateConnectionObject(parserObject);

        return parserObject;
    }

    @Override
    @Transactional
    public Page<Parser> getParsedCsvCollection(int page, int size, String sort, String direction) {
        Sort.Direction sortDirection = direction.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC;
        Pageable pages = PageRequest.of(page, size, sortDirection, sort);

        return parserRepository.findAll(pages);
    }

    private void validateConnectionObject(final Parser parserObject) {
        Validate.parserValidator.parsedCsvExist(parserObject != null);
    }

}
