package parserpackage.service.connection;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import parserpackage.model.entity.Connection;
import parserpackage.repository.ConnectionRepository;

@Service
public class ConnectionServiceImpl implements ConnectionService {
    public Page<Connection> connectionData;

    @Autowired
    private ConnectionRepository connectionRepository;

    @Override
    @Transactional
    public Page<Connection> findByParserId(int page, int size, String sort, String direction, String parser_id) {
        Sort.Direction sortDirection = direction.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC;
        Pageable pages = PageRequest.of(page, size, sortDirection, sort);

        if (parser_id == null) {
            connectionData = connectionRepository.findAll(pages);
        } else {
            connectionData = connectionRepository.findByParserId(parser_id, pages);
        }

        return connectionData;
    }

    @Override
    @Transactional
    public Connection createConnection(Connection connection) {
        connectionRepository.save(connection);

        return connection;
    }

}
