package parserpackage.service.connection;

import parserpackage.model.entity.Connection;
import org.springframework.data.domain.Page;

public interface ConnectionService {
    Connection createConnection(Connection connection);
    Page<Connection> findByParserId(int page, int size, String sort, String direction, String parser_id);

}
