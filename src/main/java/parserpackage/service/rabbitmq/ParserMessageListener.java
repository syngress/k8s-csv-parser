package parserpackage.service.rabbitmq;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.json.simple.parser.JSONParser;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import java.io.File;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.io.IOException;
import java.nio.file.*;

import parserpackage.configuration.RabbitConfig;
import parserpackage.model.entity.Connection;
import parserpackage.repository.ConnectionRepository;
import parserpackage.service.file_exporter.ExportJsonToDatabase;
import parserpackage.service.file_formatter.XmlFormatter;
import parserpackage.service.file_transformer.RemoveEmptyRows;
import parserpackage.service.file_transformer.CsvToJson;
import parserpackage.service.file_transformer.XsltTransformer;

@Component
public class ParserMessageListener {
    @Value("${parser.work_directory}")
    private String parserWorkDirectory;

    @Value("${parser.output_xml_file}")
    private String outputXmlFile;

    @Value("${parser.output_json_file}")
    private String outputJsonFile;

    public Page<Connection> connectionData;

    @Autowired
    private ConnectionRepository connectionRepository;

    @RabbitListener(queues = RabbitConfig.QUEUE_ORDERS)
    public void processMessage(Message message) {
        String parserMessage = new String(message.getBody());
        JSONObject jsondata = convertMessageToJson(parserMessage);
        Sort.Direction sortDirection = Sort.Direction.DESC;
        Pageable pages = PageRequest.of(0, 30, sortDirection, "source_ip");

        String parserFileName = (String) jsondata.get("parser_file_name");
        String csvPath = (String) jsondata.get("csv_path");
        String parserId = (String) jsondata.get("parser_id");
        String databaseConfiguration = (String) jsondata.get("database_configuration");

        createWorkDirectory();
        removeEmptyRows(parserFileName, csvPath, parserWorkDirectory);
        convertCsvToJson(parserFileName);
        saveConnectionToDatabase(parserId, databaseConfiguration);

        connectionData = connectionRepository.findByParserId(parserId, pages);
        connectionData.forEach(connectionObject -> {
            try {
                json2xml(connectionObject);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });

        formatXmlFile();
        transformXmlFromTemplate();
        clearWorkDirectory();
    }

    private JSONObject convertMessageToJson(String parserMessage) {
        try {
            return new JSONObject((String) new JSONParser().parse(parserMessage));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void removeEmptyRows(String parserFileName, String csvPath, String parserWorkDirectory) {
        RemoveEmptyRows.main(parserFileName, csvPath, parserWorkDirectory);
    }

    private void convertCsvToJson(String parserFileName) {
        CsvToJson csvToJson = new CsvToJson();
        csvToJson.convert(parserFileName, parserWorkDirectory, outputJsonFile);
    }

    private void saveConnectionToDatabase(String parserId, String databaseConfiguration) {
        ExportJsonToDatabase exportJsonToDatabase = new ExportJsonToDatabase();
        exportJsonToDatabase.prepare(databaseConfiguration, parserId, parserWorkDirectory, outputJsonFile);
    }

    private void json2xml(final Connection connection) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String xmlString = mapper.writeValueAsString(connection);
        try {
            xml2file(convert_json(xmlString));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void xml2file(final String formattedXmlFile) {
        try {
            java.io.FileWriter fw = new java.io.FileWriter(outputFile(), true);
            fw.write(formattedXmlFile);
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String convert_json(String json_value) {
        String xml = "<connection>";
        try {
            JSONObject jsonObject = new JSONObject(json_value);
            xml = xml + XML.toString(prepareJson(jsonObject));
        } catch (Exception e) {
            e.printStackTrace();
        }
        xml = xml + "</connection>";

        return xml;
    }

    static JSONObject prepareJson(JSONObject object) {
        JSONObject result = new JSONObject();
        object.keySet().forEach(key -> {
            String checkedKey = key.replace(" ", "_").replaceAll("[^a-zA-Z0-9_]", "");
            Object value = object.get(key);
            if (value instanceof JSONObject) {
                result.put(checkedKey, prepareJson((JSONObject) value));
            } else if (value instanceof String) {
                result.put(checkedKey, ((String) value).replace(">", "%3E"));
            } else {
                result.put(checkedKey, value);
            }
        });

        return result;
    }

    private void formatXmlFile() {
        XmlFormatter xmlFormatter = new XmlFormatter();
        try (Stream<String> lines = Files.lines(Paths.get(outputFile()))) {
            String content = lines.collect(Collectors.joining(System.lineSeparator()));
            java.io.FileWriter fw = new java.io.FileWriter(outputFile(), false);
            fw.write(xmlFormatter.format("<connections>" + content + "</connections>"));
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void transformXmlFromTemplate() {
        String[] templateData = new String[]{outputFile()};
        XsltTransformer.main(templateData);
    }

    private void createWorkDirectory() {
        try {
            FileUtils.forceMkdir(new File(parserWorkDirectory));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String outputFile() {
        return parserWorkDirectory + "/" + outputXmlFile;
    }

    private void clearWorkDirectory() {
        Path jsonOutputFilePath = FileSystems.getDefault().getPath(parserWorkDirectory);
        try {
            FileUtils.deleteDirectory(new File(String.valueOf(jsonOutputFilePath)));
        } catch (NoSuchFileException exception) {
            System.err.format("%s: no such" + " file or directory%n", jsonOutputFilePath);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

}
