package parserpackage.service.rabbitmq;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import parserpackage.configuration.RabbitConfig;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import parserpackage.model.entity.Parser;

@Service
public class ParserMessageSender {
    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public ParserMessageSender(RabbitTemplate rabbitTemplate, ObjectMapper objectMapper) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendMessageData(Parser parser, String csvPath, String databaseConfiguration) {
        JSONObject jsonData = new JSONObject()
                .put("parser_file_name", parser.getLog_file_name())
                .put("parser_id", parser.getId())
                .put("csv_path", csvPath)
                .put("database_configuration", databaseConfiguration);

        try {
            this.rabbitTemplate.convertAndSend(RabbitConfig.QUEUE_ORDERS, jsonData.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
