package parserpackage.validator;

import parserpackage.exception.CommonRestException;
import parserpackage.exception.ExceptionKey;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Collection;

public class Validate {
    public static final ParserValidator parserValidator = new ParserValidator();

    public static void isTrue(boolean condition, ExceptionKey exceptionKey, String description, Object... obj) {
        if (!condition) {
            throw new CommonRestException(exceptionKey.getHttpStatus(), exceptionKey.name(), String.format(description, obj));
        }
    }

    public static void isNotNull(Object value, ExceptionKey exceptionKey, String message, Object... values) {
        if (value == null) {
            throw new CommonRestException(exceptionKey.getHttpStatus(), exceptionKey.name(), String.format(message, values));
        }
    }

    public static void isNotEmpty(Collection<?> collection, ExceptionKey exceptionKey, String message, Object... values) {
        isTrue(CollectionUtils.isNotEmpty(collection), exceptionKey, message, values);
    }

}
