package parserpackage.validator;

import static parserpackage.exception.ExceptionKey.notFound;
import static parserpackage.validator.Validate.isTrue;

public class ParserValidator {
    public void inputFileExist(Boolean parsedCsvObject) {
        isTrue(parsedCsvObject, notFound, "Input file not found");
    }

    public void parsedCsvExist(Boolean parsedCsvObject) {
        isTrue(parsedCsvObject, notFound, "Parsed CSV object not found");
    }

}
